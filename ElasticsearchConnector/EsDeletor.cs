﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticsearchConnector
{
    public class EsDeletor
    {
        private EsSelector esSelector;

        public EsDeletor(EsSelector esSelector)
        {
            this.esSelector = esSelector;
        }

        public bool DeleteItemsById(string index, string type, List<string> idList)
        {
            var delResult =
                esSelector.ElConnector.DeleteByQuery<Dictionary<string, object>>(
                    ix => ix.Index(index).Type(type).Query(q => q.Ids(idList)));

            return delResult.RequestInformation.Success;

        }
    }
}
