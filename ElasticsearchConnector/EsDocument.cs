﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElasticsearchConnector
{
    [ElasticType(Name = "EsDocument")]
    public class EsDocument
    {    
        public string Id { get; set; }
        public Dictionary<string, object> Dict { get; set; }   
    }
}
