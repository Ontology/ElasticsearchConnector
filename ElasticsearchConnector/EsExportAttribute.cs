﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElasticsearchConnector
{
    public class ElExportAttribute : Attribute
    {
        public bool Exclude { get; set; }
    }
}
