﻿using Nest;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticsearchConnector
{
    public class EsSelector
    {
        public ElasticClient ElConnector { get; private set; }

        public string Server { get; private set; }
        public int Port { get; private set; }

        private string index;
        public string Index
        {
            get { return index; }
            set
            {
                if (index != value)
                {
                    index = value;
                    initialize_Client();
                }

            }
        }

        public int SearchRange { get; private set; }
        public string Session { get; private set; }
        public List<string> SpecialCharacters_Read { get; set; }

        public bool Paging { get; set; }
        public int LastPos { get; set; }
        public int PageCount { get; set; }
        public int CurPage { get; set; }
        public long Total { get; set; }

        private void initialize_Client()
        {
            var uri = new Uri("http://" + Server + ":" + Port.ToString());

            var settings = new ConnectionSettings(uri).SetDefaultIndex(Index);
            ElConnector = new ElasticClient(settings);

            try
            {
                var indexSettings = new IndexSettings();
                ElConnector.CreateIndex(Index);

            }
            catch (Exception ex)
            {

                throw new Exception("Cannot create Index!");
            }
        }

        public List<string> GetEsTypes(string strIndex = null)
        {

            var strTypes = new List<string>();
            var result = ElConnector.Search<dynamic>(s => s.Index(Index).Type("doctypes").QueryString("doctype:*").From(0).Size(1));
            //var result = ElConnector.Search(s => s.Index(Index).Type("doctypes").QueryString("doctype:*").From(0).Size(10000));
            if (result.Documents.Any())
            {
                strTypes.AddRange(result.Documents.Select(objType => objType["doctype"].ToString()).Cast<string>());
            }


            return strTypes;
        }

        public EsSelector(string server,
                          int port,
                          string index,
                          int searchRange,
                          string session)
        {
            this.Server = server;
            this.Port = port;
            this.SearchRange = searchRange;
            this.Session = session;
            this.Index = index;


            SpecialCharacters_Read = new List<string> { "\\", "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":" };

            LastPos = 0;

        }

        public List<FieldItem> FieldItems
        {
            get; set;
        }

        public List<EsDocument> GetData_Documents(string type, string index = null, bool paging = false, int lastPos = 0, string query = null)
        {
            LastPos = lastPos;
            Paging = paging;

            var Documents = new List<EsDocument>();
            long intCount = SearchRange;
            int intPos = LastPos;

            while (intCount > 0)
            {
                intCount = 0;
                var request = new GetMappingRequest(index, new TypeNameMarker().Name = type);
                var mappings = ElConnector.GetMapping(request);
                FieldItems = mappings.Mapping.Properties.Select(prop => new FieldItem(prop.Value.Type.Name) { FieldName = prop.Key.Name}).ToList();
                var result = ElConnector.Search<dynamic>(s => s.Index(index ?? this.index).Type(type).QueryString(query ?? "*").From(intPos).Size(SearchRange));
                Total = result.Total;
                
                var docs = result.Hits.Select(h => new EsDocument { Dict = new JObject(h.Source).ToObject<Dictionary<string, object>>(), Id = h.Id }).ToList();
                
                Documents.AddRange(docs);

                intCount = Total - intPos;
                intPos += docs.Count;
                if (Paging)
                {
                    var pageCount = (double)result.Total / SearchRange;
                    PageCount = (int)Math.Ceiling(pageCount);
                    CurPage = LastPos / SearchRange;
                    LastPos += SearchRange;
                    break;
                }
            }

            return Documents;
        }
    }
}
