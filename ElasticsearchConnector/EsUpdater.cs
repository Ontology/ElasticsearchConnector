﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticsearchConnector
{
    public class EsUpdater
    {
        private EsSelector esSelector;

        public EsUpdater(EsSelector esSelector)
        {
            this.esSelector = esSelector;
        }

        public bool SaveDocs(List<EsDocument> documents, string type)
        {

            esSelector.ElConnector.Flush(f => f.Index(esSelector.Index));

            var objBulkDescriptor = new BulkDescriptor();

            foreach (var objDocument in documents)
            {
                if (objDocument.Id == null) objDocument.Id = Guid.NewGuid().ToString();

                objBulkDescriptor.Index<Dictionary<string, object>>(i => i.Id(objDocument.Id).Document(objDocument.Dict).Type(type));

            }

            var bulkResult = esSelector.ElConnector.Bulk(b => objBulkDescriptor);
            return bulkResult.Items.Any(it => it.Error != null);

        }

        public bool SaveDoc<TType>(List<TType> documents, string idField, string type)
        {
            esSelector.ElConnector.Flush(f => f.Index(esSelector.Index));

            var objBulkDescriptor = new BulkDescriptor();

            var idProperty = typeof(TType).GetProperty(idField);
            var propertiesPre = typeof(TType).GetProperties().ToList();


            var properties = propertiesPre.Select(prop =>
            {
                var attributes = prop.GetCustomAttributes(typeof(ElExportAttribute), true);
                if (!attributes.Any())
                {
                    return prop;
                }

                if (!(attributes.First() as ElExportAttribute).Exclude)
                {
                    return prop;
                }

                return null;

            }).Where(prop => prop != null).ToList();




            foreach (var item in documents)
            {
                var dict = new Dictionary<string, object>();
                string id = Guid.NewGuid().ToString().Replace("-", "");
                var idObject = idProperty.GetValue(item, null);
                if (idObject != null)
                {
                    id = idObject.ToString();
                }

                foreach (var property in properties)
                {
                    var value = property.GetValue(item, null);
                    if (value != null)
                    {
                        dict.Add(property.Name, value);
                    }
                }

                objBulkDescriptor.Index<Dictionary<string, object>>(i => i.Id(id).Document(dict).Type(type));

            }

            var bulkResult = esSelector.ElConnector.Bulk(b => objBulkDescriptor);
            return bulkResult.Items.Any(it => it.Error != null);
        }
    }
}
