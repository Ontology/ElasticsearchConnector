﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticsearchConnector
{
    public class FieldItem
    {
        public string FieldName { get; set; }
        public Type FieldType { get; set; }
        public FieldItem()
        {

        }

        public FieldItem(string esType)
        {
            if (esType == "boolean")
            {
                FieldType = typeof(bool);
            }
            else if (esType == "string")
            {
                FieldType = typeof(string);
            }
            else if (esType == "long")
            {
                FieldType = typeof(long);
            }
            else if (esType== "date" || esType == "dateOptionalTime")
            {
                FieldType = typeof(DateTime);

            }
            else if (esType == "double")
            {
                FieldType = typeof(Double);
            }
            else
            {
                FieldType = typeof(string);
            }
        }
    }
}
